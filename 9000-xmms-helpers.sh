#!/bin/bash

playThisTune () {
    xmms2 stop
    xmms2 clear
    xmms2 add "${@:-.}"
    sleep 3
    xmms2 playlist sort
    xmms2 play
    xmms2 list

    return 0
}


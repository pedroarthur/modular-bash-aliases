INSTALL_PATH = $(HOME)/.bash_aliases
LIB_DIR = $(HOME)/.bashrc.d

install:
	[ -L "$(INSTALL_PATH)" ] || ln -s "$(CURDIR)/bash_aliases" "$(INSTALL_PATH)"
	[ -L "$(LIB_DIR)"      ] || ln -s "$(CURDIR)"              "$(LIB_DIR)"

clean:
	rm *~


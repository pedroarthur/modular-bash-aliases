#!/bin/bash

SIGNAL_DIRECTORY="$TMP_ROOT/local-bash-signals"

mkdir -p "$SIGNAL_DIRECTORY"

brcd::b::check_permissions () {
  stat -c "%a" "$1" | grep -qs '600' &> /dev/null
}

brcd::b::make_signal_fifo () {
  local signalFifo="$SIGNAL_DIRECTORY/$1.fifo"

  mkfifo -m a-rwx,u+rw "$signalFifo" &> /dev/null

  ! brcd::b::check_permissions "$signalFifo" && {
    >&2 echo "# $signalFifo has wrong permissions"
    return 1
  }

  echo "$signalFifo"
}

brcd::b::make_result_file () {
  local resultFile="$SIGNAL_DIRECTORY/$1.$2"
  mktemp "$resultFile".XXXXXX
}

signalingOn () {
  local signalFifo resultFile

  signalFifo="$(brcd::b::make_signal_fifo "$1")" \
    || { echo "# couldn't create fifo file"; return 2; }
  resultFile="$(brcd::b::make_result_file "$1" output)" \
    || { echo "# couldn't create temp file"; return 1; }

  trap 'trap -- RETURN; rm $resultFile;' RETURN

  shift

  datedecho "# will run: $*"
  brcd::errno "${@}" | tee "$resultFile"

  (( ${brcd_errno:=0} == 0 )) && {
      datedecho  "# done; will signal at $signalFifo"
      cat "$resultFile" > "$signalFifo"
      datedecho  "# signal sent; returning"
  }

  return "$brcd_errno"
}

onSignal () {
  local signalFifo resultFile

  signalFifo="$(brcd::b::make_signal_fifo "$1")" \
    || { echo "# couldn't create fifo file"; return 2; }
  resultFile="$(brcd::b::make_result_file "$1" input)" \
    || { echo "# couldn't create temp file"; return 1; }

  echo "# will wait signal in $signalFifo" && shift

  brcd::errno cat  "$signalFifo" > "$resultFile"

  echo "# received signal at $resultFile"
  trap 'trap -- RETURN; rm $resultFile;' RETURN

  echo "# will call: ${*} < $resultFile"
  brcd::errno trapped "${@}" < "$resultFile"
  rm "$resultFile" && trap -- RETURN

  echo "# done: ${*} < $resultFile"
  echo "# return code was ${brcd_errno:=0}"

  (( ${brcd_errno:-0} != 130 )) \
    && return "${brcd_errno}" \
    || return 0
}


onSignals () {
  while onSignal "${@}" || true
  do
    :
  done
}

__onSignal () {
  [[ $COMP_CWORD -gt 1 ]] && _command_offset 2
}

complete -F __onSignal onSignal
complete -F __onSignal onSignals
complete -F __onSignal signalingOn

LOCKS_DIRECTORY="$TMP_ROOT/local-bash-locks"

mkdir -p "$LOCKS_DIRECTORY"

lock () {
    lockName="${LOCKS_DIRECTORY}/${1}"

    while ! mkdir "${lockName}" &> /dev/null
    do
        inotifywait -e "delete,delete_self" "${lockName}" &> /dev/null
    done

    trap 'rmdir ${lockName}' EXIT

    return 0
}

unlock () {
    lockName="${LOCKS_DIRECTORY}/${1}"
    rmdir "${lockName}"
}

withDate.s () {
  timeWithSeconds.awk
}

withDate.ms () {
  timeWithMilliseconds.awk
}

BRCD_DATE_PRECISSION="Seconds"

withDate () {
  timeWith${BRCD_DATE_PRECISSION}.awk
}

datedecho () {
  echo "${@}" | withDate
}

purgeBackUps () {
  if [[ $# -gt 1 ]]
  then
    find "${@}" -name '*~'  -print0 | xargs -0 -l1 -t -r rm
  else
    find . -name '*~'  -print0 | xargs -0 -l1 -t -r rm
  fi
}

proxyless () {
  local cmd=(unset http_proxy https_proxy \; "${@}")
  bash -c "${cmd[*]}"
}

complete -F _find purgeBackUps
complete -F _command proxyless


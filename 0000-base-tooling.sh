#!/bin/bash

source::if_executable "$(brcd::aliases_default_directory)/lib/assertions.sh"
source::if_executable "$(brcd::aliases_default_directory)/lib/strings.sh"
source::if_executable "$(brcd::aliases_default_directory)/lib/echoing.sh"
source::if_executable "$(brcd::aliases_default_directory)/lib/collections.sh"

source::if_executable "$(brcd::aliases_default_directory)/lib/errno.sh"
source::if_executable "$(brcd::aliases_default_directory)/lib/os.sh"
source::if_executable "$(brcd::aliases_default_directory)/lib/processes.sh"
source::if_executable "$(brcd::aliases_default_directory)/lib/grep.sh"
source::if_executable "$(brcd::aliases_default_directory)/lib/prompt_command.sh"
source::if_executable "$(brcd::aliases_default_directory)/lib/python.sh"

source::if_executable "$(brcd::aliases_default_directory)/lib/tty_environment.sh"
source::if_executable "$(brcd::aliases_default_directory)/lib/window_rename.sh"
source::if_executable "$(brcd::aliases_default_directory)/lib/tmux_try_starting_session.sh"
source::if_executable "$(brcd::aliases_default_directory)/lib/dbus_find_session.sh"

source::if_executable "$(brcd::aliases_default_directory)/lib/history_control.sh"
source::if_executable "$(brcd::aliases_default_directory)/lib/ps1_control.sh"

source::if_executable "$(brcd::aliases_default_directory)/lib/vi_edit_mode.sh"
source::if_executable "$(brcd::aliases_default_directory)/lib/shopts.sh"
source::if_executable "$(brcd::aliases_default_directory)/lib/editor.sh"

source::if_executable "$(brcd::aliases_default_directory)/lib/path_set_up.sh"
source::if_executable "$(brcd::aliases_default_directory)/lib/man_path_set_up.sh"
source::if_executable "$(brcd::aliases_default_directory)/lib/tmp_set_up.sh"
source::if_executable "$(brcd::aliases_default_directory)/lib/git_exclusion_set_up.sh"

source::if_executable "$(brcd::aliases_default_directory)/lib/aliases.sh"
source::if_executable "$(brcd::aliases_default_directory)/lib/less.sh"
source::if_executable "$(brcd::aliases_default_directory)/lib/directory_bootstrap.sh"

source::if_executable "$(brcd::aliases_default_directory)/lib/tmux_process_helpers.sh"
source::if_executable "$(brcd::aliases_default_directory)/lib/tmux_ssh_helpers.sh"

source::if_executable "$(brcd::aliases_default_directory)/lib/files.sh"

source::if_executable "$(brcd::aliases_default_directory)/lib/environment_control.sh"
source::if_executable "$(brcd::aliases_default_directory)/lib/workspace_control.sh"

source::if_executable "$(brcd::aliases_default_directory)/lib/datetime.sh"
source::if_executable "$(brcd::aliases_default_directory)/lib/encodings.sh"

source::if_executable "$(brcd::aliases_default_directory)/lib/fortune_show.sh"
source::if_executable "$(brcd::aliases_default_directory)/lib/kubectl.sh"
source::if_executable "$(brcd::aliases_default_directory)/lib/terminus.sh"
source::if_executable "$(brcd::aliases_default_directory)/lib/proxying.sh"

source::if_executable "$(brcd::aliases_default_directory)/lib/python_venv.sh"

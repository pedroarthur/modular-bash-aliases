#!/bin/bash

export reset_color='\e[0m'
export info_color='\e[1;34m'
export error_bold_color='\e[1;31m'

levelecho () {
  color="$1"; shift
  level="$1"; shift

  echo -e "${color}${level}:${reset_color} ${*}"
}

infecho () {
  levelecho "${info_color}" "INFO" "${*}"
}

errorboldecho () {
  levelecho "${error_bold_color}" "ERROR" "${*}"
}

export -f \
  levelecho \
  errorboldecho \
  infecho \
  ;

source () {
  builtin source "${@}" || {
    local exit_code=$?
    errorboldecho "error while loading ${*}"
    return "${exit_code}"
  }
}

_brcd::source::assert_exists () {
  local candidate="${1?need a file to read}"

  ! [[ -e $candidate ]] && {
    >&2 errorboldecho "file or directory does not exist: $candidate"
    return 1
  }

  return 0
}

_brcd::source::curr_user_can_execute () {
  local candidate="${1?need a file to check}"; shift
  [[ -x ${candidate} ]]
}

_brcd::source::curr_user_can_read () {
  local candidate="${1?need a file to check}"; shift
  [[ -r ${candidate} ]]
}

source::if () {
  local check="${1?need a check function}"; shift
  local candidate="${1? need a file to check}"; shift

  _brcd::source::assert_exists "${candidate}" || return 1

  "${check}" "${candidate}" || return 2


  # shellcheck disable=SC1090
  source "${candidate}"
}

source::if_readable () {
  source::if _brcd::source::curr_user_can_read "${@}"
}

source::if_executable () {
  source::if _brcd::source::curr_user_can_execute "${@}"
}

source::load_directory () {
  local directory="${1?need a target directory}"
  local source_load_directory_debug="${source_load_directory_debug:-false}"

  ! [[ -d $directory ]] && return 0

  [[ ${source_load_directory_debug} == "true" ]] && {
    local message="loading ${directory}"
    infecho "${message}" 2> /dev/null || echo "INFO: ${message}"
  }

  if ! BRCD_SCRIPT_ROOT="$(dirname -- "${directory}")"
  then
    return 1
  fi
  export BRCD_SCRIPT_ROOT

  local file
  for file in "$directory"/*.sh
  do
    source::if_executable "$file"
  done

  unset BRCD_SCRIPT_ROOT
}

#!/bin/bash

shopt -s histappend
shopt -s histverify

export HISTSIZE=-1
export HISTFILESIZE=-1
export HISTCMD=-1
export HISTCONTROL=ignoreboth

if [[ $BRCD_HISTORY_PRIVATE == "true" ]]
then
  q () {
    session="${BRCD_TERM_PID}-${BRCD_TERM_CMD}"
    mkdir -p "/tmp/$USER/$session/restart"
    exit 0
  }
fi

p () {
  while true; do
    session="${BRCD_TERM_PID}-${BRCD_TERM_CMD}"
    if ! hctl private; then
      return $?
    elif [[ -e "/tmp/$USER/$session/restart" ]]
    then
      echo "You asked to restart... So here it is"
      rmdir "/tmp/$USER/$session/restart"
      continue
    else
      break
    fi
  done
}

h () {
  (($# >  0)) && hctl::filter "${@}"
  (($# == 0)) && history
}

hctl () {
  local subcommand=$1; shift
  "hctl::${subcommand}" "${@}"
}

hctl::delete () {
  while [[ -n ${1} ]]
  do
    history -d "${1}"
    shift
  done
}

hctl::private () {
  local temp_history
  local return_code

  temp_history=$(mktemp) || return 1

  ! [[ -f $temp_history ]] && {
    errecho "couldn't create temp file $temp_history"
    return 127
  }

  infecho "entering private history shell $temp_history"
  history -w "$temp_history.load"
  ({
    # shellcheck disable=2030
    export HISTFILE="$temp_history"
    # shellcheck disable=2030
    export BRCD_HISTORY_PRIVATE=true

    export APPLY_PS1='private \$ '
    bash
  }) || return_code=$?

  rm "$temp_history"
  infecho "exiting private history shell $temp_history"

  return "${return_code:-0}"
}

hctl::load () {
  # shellcheck disable=2031
  ${BRCD_HISTORY_PRIVATE:=false} && {
    return
  }

  local reference="${1:-${PWD}}"; shift
  local local_history

  local_history="$(realpath "${reference}/.bash_history_brcd_local")" || return 2

  # shellcheck disable=2031
  [[ -w ${local_history} ]] && [[ "${local_history}" != "${HISTFILE}" ]] && {
    infecho "using directory-specific history $local_history"
    HISTFILE="$local_history"
    export HISTFILE
  }
}

hctl::load_if_default () {
{ _hctl::is_default_brcd_history || _hctl::is_default_bash_history; } && hctl load "${BRCD_SCRIPT_ROOT}"
}

hctl::filter () {
  _hctl::history_number_tuples_grep_pcre "${@}" | _hctl::history_number_tuple_invert
}

hctl::filter-uniq () {
  _hctl::entries_grep_pcre "${@}" | sort | uniq
}

#

_hctl::history_number_tuples_grep_pcre () {
  _hctl::history_number_tuples | _hctl::grep_pcre_list "${@}"
}

_hctl::entries_grep_pcre () {
 _hctl::entries | brcd::whitespace_strip | _hctl::grep_pcre_list "${@}"
}

_hctl::history_number_tuples () {
  history | _hctl::number_history_tuple_invert
}

_hctl::grep_pcre_list () {
  local parameters=( )

  while [[ $1 == -* ]]
  do
    local grep_cli_option="$1"; shift
    parameters+=("$grep_cli_option")
  done

  local pattern="$1"; shift

  grep --color=always -P "${parameters[@]}" "${pattern}" | _hctl::grep_pcre_list_pipe_next "${@}"
}

_hctl::grep_pcre_list_pipe_next () {
  if (( $# > 0 ))
  then
    _hctl::grep_pcre_list "${@}"
  else
    cat
  fi
}


_hctl::entries () {
  history | _hctl::number_history_tuple_to_entry
}

_hctl::number_history_tuple_to_entry () {
  sed -r 's/^\s*[0-9]+\s+//'
}

_hctl::number_history_tuple_invert () {
  sed -r 's/^\s*([0-9]+)\s+(.*)/\2=:hctl::filter::sep:=\1/'
}

_hctl::history_number_tuple_invert () {
  awk -F '=:hctl::filter::sep:=' '{printf "%6d\t%s\n", $2, $1}'
}

_hctl::local_brcd_file () {
  realpath "${reference}/.bash_history_brcd_local"
}

_hctl::default_brcd_history () {
  realpath "$HOME/.bash_history_brcd"
}

_hctl::is_default_bash_history () {
  [[ $HISTFILE == "$HOME/.bash_history" ]]
}

_hctl::is_default_brcd_history () {
  [[ $HISTFILE == $(_hctl::default_brcd_history) ]]
}

_hctl::load_default () {
  ! _hctl::is_default_bash_history && {
    if [[ -r "$HISTFILE.load" ]]
    then
      history -c
      history -r "$HISTFILE.load"
      rm "$HISTFILE.load"
    fi
    return 0
  }

  if HISTFILE=$(_hctl::default_brcd_history)
  then
    export HISTFILE
    history -r
  else
    errecho "couldn't set the history file"
  fi
}

_hctl::load_default

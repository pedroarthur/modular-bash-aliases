from argparse import ArgumentParser
from sys import stdout, stderr, stdin, argv
from os import execvp, environ

debug_mode = False


def print_to_stderr(*args, **kws):
    default_print_args = {'file': stderr}
    effective_print_args = {**default_print_args, **kws}
    if (stdin.isatty() and stdout.isatty()) or debug_mode:
        print(*args, **effective_print_args)


def namespace_from_environment():
    source = ["namespace", "k8s_namespace"]
    for key in source:
        if key in environ:
            return key, environ[key]
        if key.upper() in environ:
            return key.upper(), environ[key.upper()]
    return None, None


def render_namespace(namespace, has_all_namespaces):
    source, environment_namespace = namespace_from_environment()

    if has_all_namespaces:
        if environment_namespace:
            print_to_stderr(f'WARN: ignoring K8S_NAMESPACE from {source} == {environment_namespace} because we found --all-namespace in the CLI')
        return ['--all-namespaces']

    if namespace:
        if environment_namespace:
            print_to_stderr(f'WARN: ignoring K8S_NAMESPACE from {source} == {environment_namespace} because we found --namespace {namespace} in the CLI')
        return ['--namespace', namespace]
    elif environment_namespace:
        print_to_stderr(f'INFO: using K8S_NAMESPACE from {source} == {environment_namespace}')
        return ['--namespace', environment_namespace]
    else:
        return []


def render_context_flag(command):
    if command in {'kubectl', 'istioctl'}:
        return '--context'
    else:
        return '--kube-context'


def context_from_environment():
    source = ["context", "k8s_context"]
    for key in source:
        if (key in environ) and (value := environ[key]):
            return key, value
        if (key.upper() in environ) and (value := environ[key.upper()]):
            return key.upper(), environ[key.upper()]
    return None, None


def render_context(context, context_flag):
    source, environment_context = context_from_environment()

    if context:
        if environment_context:
            print_to_stderr(f'WARN: ignoring K8S_CONTEXT from {source} == {environment_context} because we found {context_flag} {context} in the CLI')
        return [context_flag, context]
    elif environment_context:
        print_to_stderr(f'INFO: using K8S_CONTEXT from {source} == {environment_context}')
        return [context_flag, environment_context]
    else:
        return []


def render_full_cli(command, cli, context, namespace):
    if command == 'kubectl':
        if len(cli) > 0 and cli[0].startswith('-'):
            return [command] + context + namespace + cli
        if len(cli) > 1 and cli[0] == 'get' and cli[1].startswith('namespace'):
            return [command] + cli[:1] + context + cli[1:]
        else:
            return [command] + cli[:1] + context + namespace + cli[1:]
    else:
        return [command] + context + namespace + cli


def render_cli(program, decorator_arguments, other_arguments):
    context = render_context(decorator_arguments.context, render_context_flag(program))
    namespace = render_namespace(decorator_arguments.namespace, decorator_arguments.all_namespaces)

    return render_full_cli(program, other_arguments, context, namespace)


def argument_parser(argv):
    context_flags = ['-c', '--context']
    all_namespaces_flags = ['-A', '--all-namespaces', '--all']

    if argv[1] != 'kubectl':
        context_flags = ['--kube-context']
        all_namespaces_flags = ['-A', '--all-namespaces']

    parser = ArgumentParser(add_help=False, allow_abbrev=False)

    parser.add_argument(*context_flags, required=False)
    parser.add_argument(*all_namespaces_flags, action='store_true')
    parser.add_argument('-n', '--namespace', required=False)
    parser.add_argument('--brcd-debug-decorator', action='store_true')

    return parser


if __name__ == '__main__':
    parser = argument_parser(argv)

    decorator_arguments, (program, *other_arguments) = parser.parse_known_args()
    if argv[1] != 'kubectl':
        decorator_arguments.context = decorator_arguments.kube_context
    debug_mode = debug_mode or decorator_arguments.brcd_debug_decorator
    full_cli = render_cli(program, decorator_arguments, other_arguments)

    print_to_stderr('INFO: CLI augmented to:', ' '.join(full_cli))

    stderr.flush()
    stdout.flush()

    execvp(program, full_cli)

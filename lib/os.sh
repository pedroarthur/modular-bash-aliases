#!/bin/bash

brcd::os::is () { [[ $1 == $(uname -o) ]]; }

brcd::os::is_cygwin   () { brcd::os::is Cygwin   ; }
brcd::os::is_gnulinux () { brcd::os::is GNU/Linux; }

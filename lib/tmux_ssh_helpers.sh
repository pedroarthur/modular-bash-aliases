#!/bin/bash

export BRCD_SSH_SESSION="default"
export BRCD_SSH_REMOTE_TMUX=(tmux)

brcd::ssh::pre_start () {
  [[ -z $TMUX ]] && return 0

  echo "tmsh: removing status and setting prefix to c-a"
  tmux set status off
  tmux set prefix None
  tmux set key-table off
}

brcd::ssh::post_stop () {
  [[ -z $TMUX ]] && return 0

  echo "tmsh: adding status and setting prefix to c-b"

  tmux set status on
  tmux set -u prefix
  tmux set -u key-table
}

brcd::ssh::mosh () {
  local maybe="--"

  grep -qs -- -- <<< "${@}" && local maybe=""

  mosh "$maybe" "${@}" "${BRCD_SSH_REMOTE_TMUX[@]}" new-session -A -s "$USER@$HOSTNAME/${BRCD_SSH_SESSION}"
}

brcd::ssh::ssh () {
  ssh -t "${@}" "${BRCD_SSH_REMOTE_TMUX[@]}" new-session -A -s "$USER@$HOSTNAME/${BRCD_SSH_SESSION}"
}

brcd::ssh::run () {
  local impl=$1; shift

  brcd::ssh::pre_start
  brcd::errno "${impl}" "${@}"
  brcd::ssh::post_stop

  return "${brcd_errno:-0}"
}

tssh () {
  brcd::ssh::run brcd::ssh::ssh "${@}"
}

tmsh () {
  brcd::ssh::run brcd::ssh::mosh "${@}"
}

_load_completion () {
    local actual_function="$1"
    local completion_file="$2"
    local actual_command="$3"

    if ! typeset -f "$actual_function" &> /dev/null
    then
        . "$completion_file"
    fi

    complete -F "$actual_function" "$actual_command"
}

_tssh_load_ssh_completion () {
    _load_completion _ssh /usr/share/bash-completion/completions/ssh tssh
}

_tmsh_load_mosh_completion () {
    _load_completion _mosh /usr/share/bash-completion/completions/mosh tmsh
}

complete -F _tssh_load_ssh_completion tssh
complete -F _tmsh_load_mosh_completion tmsh


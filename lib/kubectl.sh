#!/bin/bash

brcd::bash::symbol_exist () { type "${@}" &> /dev/null; }

brcd::kubectl::load_if_kubectl_is_in_path () {

  brcd::bash::symbol_exist kubectl || {
    return 0
  }

  brcd::kubectl  () { python3 -B "$(brcd::aliases_default_directory)/lib/kubectl.py" kubectl  "${@}"; }
  brcd::helm     () { python3 -B "$(brcd::aliases_default_directory)/lib/kubectl.py" helm     "${@}"; }
  brcd::helmfile () { python3 -B "$(brcd::aliases_default_directory)/lib/kubectl.py" helmfile "${@}"; }
  brcd::istioctl () { python3 -B "$(brcd::aliases_default_directory)/lib/kubectl.py" istioctl "${@}"; }

  kubectl  () { brcd::kubectl  "${@}"; }
  helm     () { brcd::helm     "${@}"; }
  helmfile () { brcd::helmfile "${@}"; }
  istioctl () { brcd::istioctl "${@}"; }

  export -f kubectl  brcd::kubectl
  export -f helm     brcd::helm
  export -f helmfile brcd::helmfile
  export -f istioctl brcd::istioctl

  # materializing kubectl completions if kubectl
  # is newer than the completion file we have
  brcd::kubectl::maybe_make_completion_file () {
    local kubectl_completions="${1?completion file name is mandatory}"; shift
    local kubectl_path

    kubectl_path=$(which kubectl) || { return 1; }

    [[ "${kubectl_completions}" -nt "${kubectl_path}" ]] && return 0

    infecho kubectl: reloading completions

    mkdir -p "$(dirname "${kubectl_completions}")"

    "${kubectl_path}" completion bash > "${kubectl_completions}"
  }

  brcd::kubectl::load_completions () {
    local kubectl_completions="${HOME}/.cache/brcd/kubectl/completions"
    brcd::kubectl::maybe_make_completion_file "${kubectl_completions}"
    source::if_readable "${kubectl_completions}"
  }

  brcd::kubectl::load_completions
}

#post_cd_hooks+=(brcd::kubectl::load_if_kubectl_is_in_path)
brcd::prompt_command_add brcd::kubectl::load_if_kubectl_is_in_path

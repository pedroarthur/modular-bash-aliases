#!/bin/bash

brcd::fortune::show_banner () {
  silenced which fortune || return 0

  echo
  fortune 2> /dev/null \
    | awk '{print "  " $0}'
  echo
}

brcd::fortune::show_banner_if_level_permits () {
  [[ $BRCD_SHELL_LEVEL -lt 3 ]] && {
    brcd::fortune::show_banner
  }

  return 0
}

brcd::fortune::show_banner_if_level_permits

#!/usr/bin/env bash

if brcd::os::is "Android"
then
  ls_default_arguments=(--color=auto -gph)
else
  ls_default_arguments=(--color=auto --time-style=long-iso -goph --group-directories-first)
fi

alias ls='ls ${ls_default_arguments[*]}'

subtitlesGc ()
{
    find . -type d | while read -r d; do
        fcount=$(ls -l "$d" | wc -l);
        scount=0;
        scount=$(ls -l "$d"/*.srt 2> /dev/null | wc -l || echo 0);
        test $scount -eq $((fcount-1)) && find "$d" -not -type d -print0;
    done | xargs -0 rm;
    find . -type d -empty -print0 | xargs -0 rmdir
}

brcd::on_change::wait_on_files () {

  local event_list=(
    "modify"
    "move"
    "create"
    "delete"
    "delete_self"
    "move_self"
  )

  local events="$(brcd::join_s , "${event_list[@]}")"

  silenced inotifywait -r -e "${events}" "${@}"
}

brcd::on_change::check_parameters () {
  grep -qP "[^[:space:]] : [^[:space:]]" <<< "${*}" \
    && return 0

  >&2 echo "Usage: ${FUNCNAME[0]} FILE ... : COMMAND"
  return 1
}

brcd::on_change::create_temp_file () {
  mktemp --tmpdir "watch.$$.XXXXXXXX"
}

brcd::on_change::next_file () {
  while [[ $1 != ":" ]]
  do
    echo "$1"; shift
  done
}

brcd::on_change::check_and_build_input () {
  brcd::on_change::check_parameters "${*}"      || return 1
  tmp_file=$(brcd::on_change::create_temp_file) || return 2

  for file in $(brcd::on_change::next_file "${@}")
  do
    ! [[ -r $file ]] && {
      >&2 echo "ERROR: $file: no such file or directory"

      rm "$tmp_file"
      return 3
    }

    echo "$file" >> "$tmp_file"
  done

  echo "$tmp_file"
}

brcd::on_change::main () {
  file="${1}"

  # ignore everything until the first ":"
  for _ in $(brcd::on_change::next_file "${@}"); do shift; done; shift

  brcd::on_change::wait_on_files --fromfile "${file}" && {
    echo "# running: ${*}"; sleep .5
    brcd::errno trapped "${@}"
  }

  return "${brcd_errno:-0}"
}


on-change ()
{
    tmp_file=$(brcd::on_change::check_and_build_input "${@}") || return $?
    brcd::on_change::main "${tmp_file}" "${@}"
}

on-changes ()
{
    tmp_file=$(brcd::on_change::check_and_build_input "${@}") || return $?
    cmd_line="${*}"

    while brcd::on_change::main "${tmp_file}" "${@}" || true
    do
      echo "# done for: ${cmd_line##*: }; watching again"
    done
}

brcd::on_change::completion ()
{
  local offset

  while (( offset++ < ${#COMP_WORDS[@]} ))
  do
    [[ ${COMP_WORDS[$offset]} == ":" ]] && break
  done

  if (( COMP_CWORD > offset++ ))
  then
    _command_offset "$((offset))"
  else
    _filedir_xspec 2> /dev/null
  fi
}

complete -F brcd::on_change::completion on-change
complete -F brcd::on_change::completion on-Changes

brcd::timestamp::from_args () {
  if [[ -z $1 ]]
  then
    stat -c @%Y <(cat /dev/null)
  else
    stat -c @%Y "$1" || return 1
  fi
}

brcd::timestamp::of () {
  brcd::timestamp::from_args "${@}" | now -f /dev/stdin
}

brcd::timestamp::apply () {
  local target="${1?I need a file to timestamp}"
  local timestamp dirpart filepart

  [[ -r $target ]] || {
    errecho "$target: No such file or directory"
    return 127
  }

  timestamp=$(brcd::timestamp::of "$1") || {
    return 2
  }

  dirpart=$(dirname "${target}")
  filepart=$(basename "${target}")

  mv "$1" "${dirpart}/${timestamp}-${filepart}"
}

brcd::timestamp::help () {
cat <<'EoF'
Usage: timestamp [command] [file]
Commands:

  apply   prepend `timestamp of` to file's name
  help    show these messages
  of      get file's timestamp
EoF
}

timestamp () {
  local op="${1:-help}"; shift

  brcd::timestamp::"$op" "${@}"
}

fl () { find "${@}" -print0 | sort -zh | xargs -l -0 /bin/ls "${ls_default_arguments[@]}" --color=always -ld | column -t -l 6; }

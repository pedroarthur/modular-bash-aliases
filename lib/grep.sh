#!/bin/bash

f () {
  local parameters=( )

  while [[ $1 == -* ]]
  do
    local grep_cli_option="$1"; shift
    parameters+=("$grep_cli_option")

    [[ $grep_cli_option == '--' ]] && break
  done

  (( $# == 0 )) && {
    cat
    return 0
  }

  local pattern="$1"; shift

  grep --color=always -P "${parameters[@]}" "${pattern}" | f "${parameters[@]}" "${@}"
}

e () {
  local parameters=(-v)

  while (( $# > 0 ))
  do
    parameters+=("$1")
    shift
  done

  f "${parameters[@]}"
}

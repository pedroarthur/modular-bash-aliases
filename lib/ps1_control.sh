#!/bin/bash

[[ -n $APPLY_PS1 ]] && { export PS1="$APPLY_PS1"; }

ps1ctl () {
  local subcommand=$1; shift
  "ps1ctl::${subcommand}" "${@}"
}

ps1ctl::command () {
  local subcommand=$1; shift
  "ps1ctl::command::${subcommand}" "${@}"
}

ps1ctl::minimal () {
  export PS1='\$ '
  [[ -n $PROMPT_COMMAND ]] && [[ -z $PROMPT_COMMAND_ORIGINAL ]] && {
    export PROMPT_COMMAND_ORIGINAL="$PROMPT_COMMAND"
  }
  unset PROMPT_COMMAND
}

ps1ctl::basic () {
  export PS1='\u@\h \$ '
  [[ -n $PROMPT_COMMAND ]] && [[ -z $PROMPT_COMMAND_ORIGINAL ]] && {
    export PROMPT_COMMAND_ORIGINAL="$PROMPT_COMMAND"
  }
  unset PROMPT_COMMAND
}

ps1ctl::command::revert () {
  export PROMPT_COMMAND="${PROMPT_COMMAND_ORIGINAL}"
  unset PROMPT_COMMAND_ORIGINAL
}

#!/bin/bash

export -a prompt_command_callbacks

__brcd_prompt_command () {
  for callback in "${prompt_command_callbacks[@]}"
  do
    "$callback" "${@}"
  done
}

brcd::prompt_command_add  () { prompt_command_callbacks+=("$1"); }
brcd::prompt_command_list () { echo "${prompt_command_callbacks[@]}"; }

grep -qs __brcd_prompt_command <<< "${PROMPT_COMMAND:=}" || {
  export PROMPT_COMMAND="${PROMPT_COMMAND:=: no command}"
  export PROMPT_COMMAND="${PROMPT_COMMAND}; __brcd_prompt_command;"
}

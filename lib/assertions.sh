#!/bin/bash

__assert_error_message () {
  local message="${1}"; shift
  local expected_exit_code="${1}"; shift
  local exit_code="${1}"; shift

>&2 cat <<EoF
assertion error: ${message}: '${@}' exited ${exit_code}; ${expected_exit_code} expected
EoF
}

assert () {
  local message="${1}"; shift
  local expected_exit_code="${1}"; shift

  "${@}"
  local exit_code=${?}

  ((expected_exit_code == exit_code)) && return 0

  __assert_error_message "${message}" "${expected_exit_code}" "${exit_code}" "${@}"

  return 1
}


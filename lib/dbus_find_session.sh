#!/bin/bash

brcd::env::set_dbus_session () {
  local user_id;

  if [ -n "$DISPLAY" ]
  then
    unset DBUS_SESSION_BUS_ADDRESS

    while [ -z "$DBUS_SESSION_BUS_ADDRESS" ]
    do
      # shellcheck disable=SC2230
      if [[ -e /run/user/$(id -u)/bus ]]
      then
        if user_id=$(id -u)
        then
          export DBUS_SESSION_BUS_ADDRESS="unix:path=/run/user/${user_id}/bus"
        fi
      elif session=$(command ls "$HOME/.dbus/session-bus/"*"-${DISPLAY/:/}" 2> /dev/null)
      then
        # shellcheck disable=SC1090
        source "$session"

        # Check if process still running
        if ! grep -qsP dbus-daemon /proc/"$DBUS_SESSION_BUS_PID"/comm
        then
          unset DBUS_SESSION_BUS_ADDRESS
          rm "$HOME/.dbus/session-bus/"*"-${DISPLAY/:/}"
        fi
      elif which dbus-launch &> /dev/null
      then
        dbus_environment=( "$(dbus-launch)" )
        # shellcheck disable=SC2163
        export "${dbus_environment[@]}"
      else
        break
      fi
    done

  fi
}

#brcd::env::set_dbus_session

#!/bin/bash

brcd::dir::exist_and_executable () {
  [[ -d $1 ]] && [[ -x $1 ]]
}

brcd::colon_set::has () {
  local variable=$1; shift
  local path=$1; shift

  grep -qsP "(^|:)${path}(:|$)" <<< "${!variable}"
}

brcd::colon_set::each () {
  local variable=$1; shift
  tr : \\n <<< "${!variable}"
}

brcd::colon_set::add () {
  local variable=$1; shift
  local path=$1; shift

  brcd::colon_set::has "${variable}" "${path}" && {
    return 1
  }

  export "${variable}=${!variable:+${!variable}:}${path}"
}

brcd::colon_set::insert () {
  local variable=$1; shift
  local path=$1; shift

  brcd::colon_set::has "${variable}" "${path}" && {
    return 1
  }

  export "${variable}=${path}${!variable:+:${!variable}}"
}

brcd::dir_set::insert_if_exist () {
  local variable=$1; shift
  local path=$1; shift

  brcd::dir::exist_and_executable "${path}" || return 2

  local next_value="${path}"
  for candidate in $(brcd::colon_set::each PATH)
  do
    if [[ "$candidate" != "$path" ]] && ! brcd::colon_set::has next_value "${candidate}"
    then
      next_value="${next_value}:${candidate}"
    fi
  done

  export "${variable}=${next_value}"
}

brcd::dir_set::add_if_exists () {
  local variable=$1; shift
  local path=$1; shift

  brcd::dir::exist_and_executable "${path}" || return 2
  brcd::colon_set::add "${variable}" "${path}"
}

brcd::dir_set::declare_from_args () {
  local variable=$1; shift

  for item in "${@}"
  do
    brcd::dir_set::add_if_exists "${variable}" "${item}"
  done
}

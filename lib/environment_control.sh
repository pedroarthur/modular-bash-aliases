#!/bin/bash -x

brcd::sdk::cli::handover () {
  local prefix=$1; shift
  local suffix=$1; shift

  "$prefix"::"$suffix" "${@}"
}

brcd_is_debug_enabled () { [[ "$-" == *x* ]]; }

brcd::sdk::cli::handover_function () {
  local suffix=$1; shift
  local debug_disable=false

  brcd_is_debug_enabled || debug_disable=true

  : brcd::sdk::cli::handover_function pre execution

  set -x
  "${FUNCNAME[1]}"::"$suffix" "${@}" || local return_code=$?
  set +x

  $debug_disable || set -x

  : brcd::sdk::cli::handover_function post execution

  return "${return_code:-0}"
}

cfgctl             () { brcd::sdk::cli::handover "brcd::cfgctl" "${@}"; }
brcd::cfgctl::aws  () { brcd::sdk::cli::handover "brcd::aws"    "${@}"; }
brcd::aws::profile () { brcd::sdk::cli::handover_function       "${@}"; }


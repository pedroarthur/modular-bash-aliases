#!/bin/bash

hasLocalX () {
    echo "$DISPLAY" | grep -E '^:[[:digit:]]+(\.[[:digit:]]+)?$' &> /dev/null
    return $?
}

hasDolphin () {
    [ "${BRCD_TERM_CMD}" == "dolphin" ]
}

getMeBackToShell () {
  if ! session="$(tmux display-message -p '#S')"
  then
    return 1
  fi

  mkdir -p "/tmp/$USER/$session/getMeBackToShell"

  tmux kill-session
}

q () {
  if ! session="$(tmux display-message -p '#S')"
  then
    return 1
  fi

  mkdir -p "/tmp/$USER/$session/restart"

  tmux kill-session
}

currentTmuxPane () {
  tmux display -pt "${TMUX_PANE:?}" '#{pane_index}'
}

brcd::tmux::reclaim () {
  if ! tmux list-session | grep -qsv '^sentinel: 1 windows'
  then
    tmux kill-server
  fi
}

brcd::tmux::sentinel () {
  #shellcheck disable=2230
  #false \
  #  || which htop 2> /dev/null \
  #  || which top 2> /dev/null \
  #  || echo "$(which watch) ps -aux --sort -rss" \
  #;
  which bash
}

brcd::tmux::start () {
  if [ -z "$TMUX" ] && (hasLocalX || brcd::os::is_cygwin) && ! hasDolphin
  then
      session="${BRCD_TERM_PID}-${BRCD_TERM_CMD}"

      tmux new-session -d -s sentinel "$(brcd::tmux::sentinel)" 2> /dev/null

      while true; do
        launchline=( )

        launchline+=(new-session  -d -s "$session" \;)
        launchline+=(set-environment -t "$session" BRCD_TERM_PID  "${BRCD_TERM_PID}" \;)
        launchline+=(set-environment -t "$session" BRCD_TERM_CMD  "${BRCD_TERM_CMD}" \;)
        launchline+=(set-environment -t "$session" BRCD_THEME     "${BAT_THEME}"     \;)

        attachline+=(attach-session -t "$session" \;)

        if ! brcd::errno tmux "${launchline[@]}"
        then
          echo "ERROR: lauching ${launchline[*]} has failed with return code ${brcd_errno:=0}"
          return "${brcd_errno}"
        elif [ -e "$HOME/.tmux.local.conf" ] && ! brcd::errno tmux source-file "$HOME/.tmux.local.conf"
        then
          echo "ERROR: source-file .tmux.local.conf has failed with return code $brcd_errno"
          return "${brcd_errno}"
        elif ! brcd::errno tmux "${attachline[@]}"
        then
          echo "ERROR: ${attachline[*]} has failed with return code $brcd_errno"
          return "${brcd_errno}"
        elif [[ -e "/tmp/$USER/$session/restart" ]]
        then
          echo "You asked to restart... So here it is"
          rmdir "/tmp/$USER/$session/restart"
          continue
        elif [ -e "/tmp/$USER/$session/getMeBackToShell" ]
        then
          echo "You asked to get to shell... So here it is"
          rmdir "/tmp/$USER/$session/getMeBackToShell"
          return 0
        elif ! brcd::errno brcd::tmux::reclaim
        then
          echo "ERROR: brcd::tmux::reclaim has failed with return code $brcd_errno"
          return "${brcd_errno}"
        else
          exit 0
        fi
      done
  elif TMUX_PANE_IDX=$(currentTmuxPane 2> /dev/null)
  then
    export TMUX_PANE_IDX
    # shellcheck disable=1090
    source <(tmux show-env | grep ^BRCD | sed 's/^/export /g')
  else
    unset TMUX_PANE_IDX
  fi
}

brcd::tmux::start

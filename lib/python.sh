#!/bin/bash

private::brcd::python::run::generator () {
  local source_code_generator=${1}; shift

  python3 -Bu <("${source_code_generator}")  "${@}"
}

#!/bin/bash

time_zones=(
  America/Sao_Paulo

  America/Los_Angeles
  America/Chicago
  America/New_York

  UTC

  Asia/Kolkata
)

time_zones_aliases=(
  BRT

  PST
  CST
  EST

  UTC

  IST
)

now () {
  TZ=${TZ:-America/Sao_Paulo} date +%Y%m%d%H%M%S "${@}"
}

export -f now

brcd::datetime::world_time_table () {
  now_tz=${TZ:-America/Sao_Paulo}
  now_hour=$(TZ="${now_tz}" date '+%H')
  now_hour=${now_hour##0}

  {
    echo "${time_zones_aliases[@]}"

    for hour in $(seq 0 23)
    do
      reference_date="$(date --iso-8601 "${@}")T$hour:00:00-0300"

      output_pre=""
      output_main=""
      output_post=""

      if ((hour == now_hour))
      then
        output_pre="${info_color:-\e[1;34m}"
        output_post="${reset_color:-\e[0m}"
      fi

      for time_zone in "${time_zones[@]}"
      do
        output_main+="$(TZ="$time_zone" date --date="$reference_date" '+%H:%M') "
      done

      echo -e "$output_pre$output_main$output_post"
      tput sgr0
    done
  } | column -t
}

time_table () {
  brcd::datetime::world_time_table "${@}"
}

brcd::datetime::world_clock_now_table () {
  echo "${time_zones_aliases[@]}"

  for offset in $(seq -2 +2)
  do
    local output_pre=
    local output_main=
    local output_post=
    local time_output_format="+%H:00"

    ((offset == 0)) && {
      output_pre="${info_color:-\e[1;34m}"
      output_post="${reset_color:-\e[0m}"
      time_output_format="+%H:%M"
    }

    for time_zone in "${time_zones[@]}"
    do
      output_main+="$(TZ="$time_zone" date --date="now $offset hours" "$time_output_format") "
    done

    echo -e "$output_pre$output_main$output_post"
    tput sgr0
  done
}

brcd::datetime::world_clock_now_banner () {
  echo
  {
    date +"%F %T %A %d %B semana %U"
    echo
    world_clock_now
  } | awk '{print "  " $0}'
  echo
}

world_clock_now () {
  brcd::datetime::world_clock_now_table | column -t
}

brcd::datetime::world_clock_now_alias () {
  if [[ -n $1 ]] || [[ ! -t 1 ]] || [[ ! -t 0 ]]
  then
    # shellcheck disable=2230
    $(which date) "${@}"
  else
    brcd::datetime::world_clock_now_banner
  fi
}

brcd::datetime::show_world_clock_if_level_permits () {
  [[ $BRCD_SHELL_LEVEL -lt 3 ]] && {
    brcd::datetime::world_clock_now_banner
  }

  return 0
}

alias date="brcd::datetime::world_clock_now_alias"

brcd::datetime::show_world_clock_if_level_permits

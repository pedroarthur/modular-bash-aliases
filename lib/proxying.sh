#!/bin/bash

export default_proxy_url='socks5h://localhost:8080'

activate-proxy () {
  local proxy_variables=(
    all_proxy
    http_proxy
    https_proxy
  )

  local effective_proxy_url="${1:-${proxy_url:-${default_proxy_url}}}"

  for proxy_variable in "${proxy_variables[@]}"
  do
    local lower_case_variable
    local upper_case_variable

    lower_case_variable=$(echo "${proxy_variable}" | tr '[:upper:]' '[:lower:]') || return 1
    upper_case_variable=$(echo "${proxy_variable}" | tr '[:lower:]' '[:upper:]') || return 1

    export "${lower_case_variable}=${effective_proxy_url}"
    export "${upper_case_variable}=${effective_proxy_url}"
  done
}

#!/bin/bash

export BRCD_TERM_PID="${BRCD_TERM_PID:=$$}"
export BRCD_TERM_CMD="${BRCD_TERM_CMD:=$(cat /proc/$PPID/comm)}"
export BRCD_SHELL_LEVEL=$((${BRCD_SHELL_LEVEL:-0} + 1))

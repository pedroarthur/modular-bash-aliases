#!/bin/bash

private::brcd::url::encode::py () {
cat <<EoF
#!/usr/bin/env python3

from sys import argv
from urllib.parse import quote

try:
  while True:
    print(quote(input()))
except EOFError:
  pass
EoF
}

brcd::url::encode () { private::brcd::python::run::generator private::brcd::url::encode::py; }

urlencode () {
  case "$#" in
    0) : use stdin ; brcd::url::encode ;;
    *) echo "${@}" | brcd::url::encode ;;
  esac
}

jwtd() {
    if [[ -x $(command -v jq) ]]; then
         jq -R 'split(".") | .[0],.[1] | @base64d | fromjson' <<< "${1}"
         echo "Signature: $(echo "${1}" | awk -F'.' '{print $3}')"
    fi
}

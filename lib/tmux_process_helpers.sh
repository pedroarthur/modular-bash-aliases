#!/bin/bash

renameTmuxSession () {
    tmux rename-session "${BRCD_TERM_PID}-${1:-${BRCD_TERM_CMD}}"
}

demuxed () {
  variables=( )

  # Remove TMUX variables
  mapfile -O "${#variables[@]}" -t variables \
    < <(env | awk -F = '/^TMUX/  {printf("%s ", $1)}')

  # Remove BRCD variables
  mapfile -O "${#variables[@]}" -t variables \
    < <(env | awk -F = '/^BRCD/ {printf("%s ", $1)}')

  ( for w in ${variables[*]}
    do
      unset "$w"
    done

    "${@}"
  )
}

complete -F _command demuxed


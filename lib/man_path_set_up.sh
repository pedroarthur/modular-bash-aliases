#!/bin/bash

brcd::env::set_man_path () {
  # shellcheck disable=SC2230
  which manpath &> /dev/null || return 0

  if [[ -z $MANPATH ]] && MANPATH=$(manpath)
  then
    export MANPATH="$MANPATH:$HOME/.local/root/usr/share/man"
    export MANPATH="$MANPATH:$HOME/.local/root/tpy/usr/share/man"
  fi
}

brcd::env::set_man_path

#!/bin/bash

new-window () {
  local systemd_args=(--user --quiet)
  local command=(terminus)

  case "$1" in
    at) :
      systemd_args+=(--working-directory "$(realpath "$2")")
      shift 2
      ;;
    here) :
      shift
      new-window at "${PWD}" "${@}"
      return $?
      ;;
    workspace) :
      shift
      command=(terminus-workspace)
      ;;
    *) :
      new-window here "${@}"
      return $?
      ;;
  esac
  systemd-run "${systemd_args[@]}" "${command[@]}" "${@}"
}

new-window-exit () {
  new-window "${@}"
  exit 0
}

new-split-exit () {
  tmux split-window
  exit 0
}

alias nw=new-window
alias wq=new-window-exit
alias sq=new-split-exit

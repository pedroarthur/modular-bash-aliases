#!/usr/bin/env bash

brcd::errno () {
  "${@}"
  export brcd_errno=$?
  return $brcd_errno
}

complete -F _command brcd::errno

#!/bin/bash

brcd::join_s () {
  local IFS="$1"
  shift
  echo "${*}"
}

brcd::whitespace_show  () { sed 's/ /·/g;s/\t/￫/g;s/\r/§/g;s/$/¶/g'; }
brcd::whitespace_strip () { sed -r -e 's/^[[:space:]]+//' -e 's/[[:space:]]+$//'; }

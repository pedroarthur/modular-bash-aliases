#!/bin/bash

readonly BRCD_WORKSPACE_CURRENT="$HOME/.config/brcd-workspace-current-workspace-root"

brcd::workspace::settings::generate () {
  realpath "$1"
}

brcd::workspace::set () {
  local target=${1:-.}
  local file="${BRCD_WORKSPACE_CURRENT}"

  brcd::workspace::settings::generate "${target}" \
    | tee "${file}"

  chmod +x "${file}"
}

brcd::workspace::get () {
  cat "${BRCD_WORKSPACE_CURRENT}" 2> /dev/null
}

workspace () {
  target=$(brcd::workspace::get) || {
    errecho "workspace is not set";
    return 1;
  }

  ! [[ -d $target ]] && {
    errecho "workspace does not exist";
    return 2;
  }

  cd "$target" || {
    errecho "cant cd"
    return 4;
  }

  infecho "entering workspace..."
  export BRCD_WORKSPACE_CHANGED=1
  exec bash
}

brcd::workspace::unset_if_changed () {
  [[ -n $BRCD_WORKSPACE_CHANGED ]] && {
    unset BRCD_WORKSPACE_CHANGED
    succecho workspace activated
  }

  return 0
}

brcd::workspace::unset_if_changed

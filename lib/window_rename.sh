#!/bin/bash

brcd::reset_window_name () {
  echo -ne "\033]0;" \
    "$(md5sum <<< "${BRCD_TERM_PID}" | cut -c1-6):" \
    "${BRCD_TERM_PID}:" \
    "${BRCD_TERM_CMD}" \
    "\007"
}

brcd::reset_window_name


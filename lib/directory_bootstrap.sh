#!/bin/bash

mkbootstrapscripts () {
  local bootstrap_script_path
  local bootstrap_directory

  bootstrap_directory=$(source::bootstrap::render_bootstrap_directory "$PWD") || return 1
  bootstrap_script_path="${bootstrap_directory}/0000-bootstrap.sh"            || return 1

  [[ -e ${bootstrap_directory} ]] && {
    warnecho "${bootstrap_directory} already exists"
    return 1
  }

  mkdir -p "${bootstrap_directory}" 2> /dev/null

  cat > "$bootstrap_script_path" <<< "#!/bin/bash"
  chmod +x "$bootstrap_script_path"

  infecho "$bootstrap_script_path created"

  return 0
}

function2bootstrap () {
  function="$1"
  filename="$(aliases::directory::bootstrap)"/9999-"${function}".sh

  typeset -f "${function}" > "${filename}"

  chmod +x "${filename}"
}

complete -F _function function2bootstrap

export post_cd_hooks=(
    source::load_directory_scripts
    brcd::reload_paths
)

bootstrap::cd::post_hooks () {
    local hook

    for hook in "${post_cd_hooks[@]}"
    do
      "$hook" || echo "post_cd_hooks: $hook: failed with $?; ignoring"
    done
}

bootstrap::cd () {
    builtin cd "${@:-$HOME}" && bootstrap::cd::post_hooks
    return $?
}

alias cd=bootstrap::cd

#!/bin/bash

export emph_color='\e[1;37m'
export error_color='\e[0;31m'
export success_bold_color='\e[1;32m'
export success_color='\e[0;32m'
export warning_color='\e[1;33m'

colorecho () {
  color="$1"; shift

  # shellcheck disable=SC2154
  echo -e "${color}${*}${reset_color}"
}

emphecho () {
  colorecho "${emph_color}" "${@}"
}

errecho () {
  levelecho "${error_color}" "ERROR" "${*}"
}

succecho () {
  levelecho "${success_color}" "SUCCESS" "${*}"
}

warnecho () {
  levelecho "${warning_color}" "WARNING" "${*}"
}

export -f \
  colorecho \
  levelecho \
  emphecho \
  errecho \
  succecho \
  warnecho \
  ;


#!/bin/bash

export brcd_path_set=false
export brcd_paths=(
  "${HOME}/.cargo/bin"
  "${HOME}/.local/bin"
  "${HOME}/.local/root/bin"
  "${HOME}/.venv/bin"
)

brcd::reload_paths () { export brcd_path_set=false; }

brcd::set_up_paths () {
  ! $brcd_path_set || return 0

  local candidate
  for candidate in "${brcd_paths[@]}"
  do
    brcd::dir_set::insert_if_exist PATH "${candidate}" || :
  done

  export brcd_path_set=true
}

#!/bin/bash

brcd::env::set_tmp_root () {
  local tempfile;
  local tmp_root;

  if tempfile=$(mktemp -u) && tmp_root=$(dirname "$tempfile")
  then
    export TMP_ROOT="$tmp_root"
  fi
}

brcd::env::set_tmp_root

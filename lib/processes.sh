#!/bin/bash

silenced   () { "${@}" &> /dev/null; }
background () { "${@}" &  disown   ; }

exiting () {
  background "${@}"
  exit 0
}

trapped () {
  trap '# ignoring SIGINT as requested' SIGINT
  trap 'trap -- SIGINT; trap -- RETURN' RETURN

  "${@}"
}

complete -F _command silenced
complete -F _command background
complete -F _command exiting
complete -F _command trapped



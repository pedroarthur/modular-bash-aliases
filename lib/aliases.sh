#!/bin/bash

if brcd::os::is "Android"
then
  alias ls="ls --color=auto -gph "
else
  alias ls="ls --color=auto --time-style=long-iso -goph --group-directories-first"
fi

alias sudo="sudo -E "

alias nonevim="vim -u NONE -N --cmd 'filetype plugin on' "
alias     vim="nvim"

alias tcpdump="tcpdump -nnvvXSs 1514"

alias clear='{ $(which clear) || true; }; { tmux clear-history &> /dev/null || true; } '

alias rsync="rsync -hv"

alias vt="vd -f tsv"

diff () { /usr/bin/diff --color=auto -W "$(tput cols)" "${@}"; }

alias sure=fuck

latex.mk () { make -f "$HOME/.local/root/lib/latex/make/latex.makefile" "${@}"; }

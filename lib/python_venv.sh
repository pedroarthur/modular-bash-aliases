#!/bin/bash

{ [[ -z $VIRTUAL_ENV ]] && [[ ! -d "$HOME/.venv" ]]; } || {
  # shellcheck disable=SC1091
  source "$HOME/.venv/bin/activate"
}

#!/bin/bash

jcurl () {
    curl \
      -s \
      -D /dev/stdout \
      -o >(python3 -m json.tool) \
      -H 'Content-Type: application/json' \
      "${@}" \
    ;
}

jcurlPut () {
    jcurl -X PUT "${@}"
}

jcurlDelete () {
    jcurl -X DELETE "${@}"
}


#!/bin/bash

brcd::aliases_default_directory () { echo "$HOME/.bashrc.d"; }

export -f brcd::aliases_default_directory

brcd::load () {
  readonly local_root_completion_directory="$HOME/.local/root/share/bash-completion/completions"
  readonly aliases_plasma_environment="$HOME/.config/plasma-workspace/env"

  # shellcheck disable=SC1091
  source "$(brcd::aliases_default_directory)/lib/loading.sh"

  source::bootstrap::render_directory_default_name () {
    echo ".bash_aliases_bootstrap"
  }

  source::bootstrap::render_bootstrap_directory () {
    local target="${1}"; shift
    echo "${target}/$(source::bootstrap::render_directory_default_name)"
  }

  source::bootstrap::from () {
    local parent
    local reference="${*}"

    declare -a sources_to_root

    while parent=$(dirname "$reference") && [[ $parent != "/" ]] && [[ $parent != "." ]]
    do
      if _brcd::source::curr_user_can_execute  "$(source::bootstrap::render_bootstrap_directory "${parent}")"
      then
        sources_to_root+=("$parent")
      fi
      reference="$parent"
    done

    while ((${#sources_to_root} > 0))
    do
      source::load_directory "$(source::bootstrap::render_bootstrap_directory "${sources_to_root[-1]}")"
      unset 'sources_to_root[-1]'
    done

    source::load_directory "$(source::bootstrap::render_bootstrap_directory "${@}")"
  }

  source::load_directory_scripts () {
    source::bootstrap::from "${PWD}"
  }

  source::load_session_scripts () {
    source::load_directory "$local_root_completion_directory"
    source::load_directory "$aliases_plasma_environment"
    source::load_directory "$(brcd::aliases_default_directory)"
  }

  source::load () {
    source::load_session_scripts
    source::load_directory_scripts
  }

  source::load
  brcd::prompt_command_add brcd::set_up_paths
}

brcd::enable     () { rm /tmp/brcd_disable 2> /dev/null; }
brcd::is_enabled () { ! [[ -r /tmp/brcd_disable ]]; }
brcd::disable    () { touch /tmp/brcd_disable; }

if brcd::is_enabled
then
  brcd::load
else
  echo skipping boodstrap because /tmp/brcd_disable exists
fi
